package com.ndichu;

import java.util.Arrays;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;


public class companypayroll extends AbstractVerticle {

    @Override
    public void start() {

        Router router = Router.router(vertx);
        router.get("/api/name").handler(this::getName);
        router.get("/api/average").handler(this::average);
        router.get("/api/smalltest").handler(this::small);
        router.get("/api/Largetest").handler(this::Largetest);

        this.vertx
            .createHttpServer()
            .requestHandler(router::accept)
            .listen(8080);

        }

private void getName(RoutingContext rc)
        {
            JsonObject obj = new JsonObject()
                .put("first", "Patrick")
                .put("middle", "Ndichu")
                .put("last", "Mburu");
                     
    
            rc.response().end(obj.encode());
        }

private void average(RoutingContext rc1)
   {
        Double[] x={23.0,15.0,-5.0,27.0,41.0,30.0,-7.0,15.0};
        
        for (int p=1; p<x.length; p++)
          {
            Double sum = 0.0;    sum += x[p];   Double avg = sum/8 ;

        JsonObject obj1 = new JsonObject()
           .put( "Average = ",avg);
         rc1.response().end(obj1.encode());
          } 
    
    }

            
                 
private void small(RoutingContext rc2) {
        int arr[] = { 23, 24, -5, 27, 41, 30, -7, 15 };
        int x = 0;
        do {
            Arrays.sort(arr);

            int min = arr[0];
             JsonObject obj3 = new JsonObject().put(" The smallest Value  = ", min);

            rc2.response().end(obj3.encode());

        } while (x < arr.length);

    }

private void Largetest (RoutingContext rc4)
          {
    int numbs[] = {23,24,-5,27,41,30,-7,15};
             int w =0;                   
                   
       do{      
        Arrays.sort(numbs);
        int max= numbs[numbs.length-1];

                   JsonObject obj7 = new JsonObject()
                   .put( "Largest Value is = ",  max);
                   rc4.response().end(obj7.encode());
                                                                                                                                         
         } while(w < numbs.length);  
                           
        }          
     



}
